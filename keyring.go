package main

import (
	"bufio"
	"os"
	"strings"

	keyring "github.com/zalando/go-keyring"
	"gitlab.com/diamondburned/go-aniscrob/oautsu"
)

var (
	// KeyringService is the keyring service name
	KeyringService = "go-aniscrob"
)

func clear() {
	print("\033[H\033[2J")
}

func prompt(q string) string {
	reader := bufio.NewReader(os.Stdin)

	clear()

	print(q + "\n\t")
	text, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}

	return strings.TrimSuffix(text, "\n")
}

func getPwd() oautsu.Credentials {
	// You can `source ANISCROB_EMAIL="email@domain"` in bashrc
	email := flags.User.Email

	if flags.Reset {
		err := keyring.Delete(KeyringService, email)
		if err != nil && err == keyring.ErrNotFound {
			println("No keyring found.")
			os.Exit(0)
		} else if err != nil {
			panic(err)
		}

		println("Keyring deleted for user", email)
		os.Exit(0)
	}

	pwd, err := keyring.Get(KeyringService, email)
	if err != nil && err == keyring.ErrNotFound {
		pwd = prompt("Enter your password:")
		clear()

		if err := keyring.Set(KeyringService, email, pwd); err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}

	return oautsu.Credentials{
		Email:    email,
		Password: pwd,
	}
}
