package main

import (
	"flag"
	"log"
	"os"
	"strconv"
	"strings"
)

// Flags contains all the possible command-line flags
type Flags struct {
	Kitsu bool
	Reset bool
	User  struct {
		ID    int
		Email string
	}
	Throttle string
	Command  string
}

func getFlags() Flags {
	flags := Flags{}

	flag.BoolVar(&flags.Kitsu, "disable-kitsu", true,
		"Fetch more information in Kitsu")
	flag.BoolVar(&flags.Reset, "reset", false,
		"Reset keyring credentials")
	flag.IntVar(&flags.User.ID, "u", 0,
		"The user ID to update the profile on Kitsu")
	flag.StringVar(&flags.User.Email, "e", "",
		"The email to load the keyring and authenticate with Kitsu")
	flag.StringVar(&flags.Throttle, "t", "2s",
		"The delay between each player queries")

	flag.Parse()

	if flags.User.ID == 0 {
		id := os.Getenv("ANISCROB_ID")

		if id == "" {
			log.Println("UserID is not set (-u flag or $ANISCROB_ID).")
			os.Exit(1)
		}

		idInt, err := strconv.Atoi(id)
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}

		flags.User.ID = idInt
	}

	if flags.User.Email == "" {
		email := os.Getenv("ANISCROB_EMAIL")

		if email == "" {
			email = prompt("Enter your username (or set $ANISCROB_EMAIL):")
			clear()
		}

		flags.User.Email = email
	}

	flags.Command = strings.Join(flag.Args(), " ")

	return flags
}
