package main

import kitsu "github.com/KurozeroPB/kitsu-go"

func (m *Media) queryMedia() (*kitsu.Anime, error) {
	anime, err := kitsu.SearchAnime(m.Name, 0)
	if err != nil {
		return nil, err
	}

	if !flags.Kitsu {
		return nil, nil
	}

	return anime, nil
}
