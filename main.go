package main

import (
	"log"
	"os"
	"time"

	"github.com/davecgh/go-spew/spew"

	"gitlab.com/diamondburned/go-aniscrob/oautsu"
)

var (
	logFile *os.File
	flags   = getFlags()
	creds   = getPwd()
	auth    = initKitsu()
	// cfg  = readConfig()
)

func initKitsu() *oautsu.OAuth {
	auth, err := creds.NewAuth()
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	oautsu.User = flags.User.ID

	return auth
}

func main() {
	logFile, err := os.OpenFile(
		os.TempDir()+"/go-aniscrob.log",
		os.O_APPEND|os.O_WRONLY|os.O_CREATE,
		0664,
	)

	if err != nil {
		panic(err)
	}

	defer logFile.Close()

	log.SetOutput(logFile)
	log.SetFlags(log.Lshortfile)

	clear()

	thrDuration, err := time.ParseDuration(flags.Throttle)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	throttle := time.Tick(thrDuration)

	oldPlayers := make(map[string]string)

	for {
		<-throttle

		players := GetPlayers()

		if len(players) == 0 {
			continue
		}

		for i, p := range players {
			o, ok := oldPlayers[p.ProcessName]
			if !ok || p.Title != o {
				oldPlayers[p.ProcessName] = p.Title

				media, err := parseName(p.Title)
				if err != nil {
					log.Println(err)
					continue
				}

				log.Println("Change detected:", spew.Sdump(media))

				anime, err := media.queryMedia()
				if err != nil {
					log.Println(err)
					continue
				}

				players[i].Anime = anime
				players[i].Media = *media

				if err := auth.AddEntry(anime, media.Episode); err != nil {
					log.Println(err)
				}
			}
		}
	}
}
