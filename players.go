package main

import (
	"errors"
	"log"
	"regexp"
	"strconv"
	"strings"

	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/ewmh"
	kitsu "github.com/KurozeroPB/kitsu-go"
	ps "github.com/mitchellh/go-ps"
)

var (
	// KnownFormats contains a list of known formats
	KnownFormats = []string{
		"mkv",
		"mp4",
	}

	// KnownPlayers contains a list of known players
	KnownPlayers = []string{
		"gnome-mpv",
		"gnome-mplayer",
		"mpv",
		"mplayer",
		"vlc",
	}

	X = initX()
)

// Players is the struct for many players
type Players []Player

// Player is the struct for a mediaplayer
type Player struct {
	ProcessName string
	Title       string
	Media       Media
	Anime       *kitsu.Anime
}

// Media is the struct containing info parsed from filenames
type Media struct {
	Name       string
	Episode    int
	Format     string
	Resolution string
	SubGroup   string
}

func initX() *xgbutil.XUtil {
	X, err := xgbutil.NewConn()
	if err != nil {
		log.Panicln(err)
	}

	return X
}

// GetPlayers returns a non-populated list of players
func GetPlayers() []Player {
	// Get a list of all client ids.
	clientids, err := ewmh.ClientListGet(X)
	if err != nil {
		log.Panicln(err)
	}

	pls := []Player{}

	for _, clientid := range clientids {
		pid, err := ewmh.WmPidGet(X, clientid)
		if err != nil {
			log.Println(err)
			continue
		}

		proc, err := ps.FindProcess(int(pid))
		if err != nil {
			log.Println(err)
			continue
		}

		procname := proc.Executable()
		if !matchAgainst(KnownPlayers, procname) {
			continue
		}

		name, err := ewmh.WmNameGet(X, clientid)
		if err != nil {
			log.Println(err)
			continue
		}

		pls = append(pls, Player{
			ProcessName: procname,
			Title:       name,
		})
	}

	return pls
}

func matchAgainst(array []string, against string) bool {
	var match = false
	for _, a := range array {
		if a == against {
			match = true
			continue
		}
	}

	return match
}

// parseName implies that the typical filename goes like
//     [Translation Group] Anime name - Anime episode [Resolution].format
func parseName(title string) (*Media, error) {
	trim := title

	format := ""
	// Get everything before the format
	for _, p := range KnownFormats {
		// r := regexp.MustCompile("\\." + p + ".*")
		// trim = r.ReplaceAllLiteralString(trim, "")

		trim = strings.Split(trim, "."+p)[0]

		if trim != title {
			format = p
			break
		}
	}

	if format == "" {
		log.Println(title)
		return nil, errors.New("Unknown format or string")
	}

	// Get translation group and resolution
	r := regexp.MustCompile("\\[.*?\\]")
	arr := r.FindAllString(trim, -1)
	if len(arr) < 2 {
		log.Println(arr)
		return nil, errors.New("Error parsing translation group and resolution")
	}

	trailingBraces := func(r rune) bool {
		return r == '[' || r == ']'
	}

	group := strings.TrimFunc(arr[0], trailingBraces)
	resolution := strings.TrimFunc(arr[1], trailingBraces)

	log.Println("Group:", group)
	log.Println("Resolution:", resolution)

	trim = strings.TrimSpace(r.ReplaceAllString(trim, ""))

	nameEp := strings.Split(trim, " - ")
	if len(nameEp) < 2 {
		log.Println(nameEp)
		return nil, errors.New("Error parsing name and episode")
	}

	ep, err := strconv.Atoi(strings.Join(nameEp[1:], " - "))
	if err != nil {
		return nil, err
	}

	log.Println("Name:", nameEp[0])
	log.Println("Episode:", ep)
	log.Println("Format:", format)

	return &Media{
		Name:       nameEp[0],
		Episode:    ep,
		Format:     format,
		Resolution: resolution,
		SubGroup:   group,
	}, nil
}
