#!/bin/bash

# A list of known players
pls=(
	# CLI
	mpv
	mplayer
	# GUI
	vlc
	gnome-mpv
	gnome-mplayer
)

IFS=$'\n'

pids=()
for pl in "${pls[@]}"; do
	pids+=( $(pgrep -f "$pl") )
	echo $pids
done

titles=()
for pid in "${pids[@]}"; do
	_p=( $(xdotool search --pid "$pid") )
	echo $_p
	for p in "${_p[@]}"; do
		titles+=( "$(xdotool getwindowname $p)" )
	done
done

printf "%s\n\n" "${titles[@]}"
