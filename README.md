# go-aniscrob

Anime Scrobbler for Linux

## Dependencies

- Some xcb dev packages, idk
- Keyring manager (usually `gnome-keyring`)
- Known players and formats

```go
var (
	// KnownFormats contains a list of known formats
	KnownFormats = []string{
		"mkv",
		"mp4",
	}

	// KnownPlayers contains a list of known players
	KnownPlayers = []string{
		"gnome-mpv",
		"gnome-mplayer",
		"mpv",
		"mplayer",
		"vlc",
	}
)
```

## Quickstart

1. Throw this into `.shellrc`:
```bash
# go-aniscrob stuff
export ANISCROB_EMAIL="<your kitsu email>"
export ANISCROB_ID="<your kitsu user ID>"
# end
```

2. Start the thing

## Information

- Logs are in `/tmp/go-aniscrob.log`. You can `tail -f` it.
- To reset passwords and/or keyring, do `./go-aniscrob reset`

## Credits

[`kitsu-go`](https://github.com/KurozeroPB/kitsu-go)

