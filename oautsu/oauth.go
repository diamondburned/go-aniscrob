package oautsu

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// NewAuth makes a new authentication
func (creds *Credentials) NewAuth() (*OAuth, error) {
	if creds.Email == "" || creds.Password == "" {
		return nil, errors.New("Email or Password blank")
	}

	credentials = *creds

	form := url.Values{}
	form.Add("grant_type", "password")
	form.Add("username", creds.Email)
	form.Add("password", creds.Password)

	request, err := http.NewRequest(
		"POST",
		"https://kitsu.io/api/oauth/token",
		strings.NewReader(form.Encode()),
	)

	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	r := dummyresponse{}

	if err := json.Unmarshal(bytes, &r); err != nil {
		return nil, err
	}

	expires, err := time.ParseDuration(strconv.Itoa(r.ExpiresIn) + "s")
	if err != nil {
		return nil, err
	}

	created := time.Unix(r.CreatedAt, 0)

	return &OAuth{
		AccessToken:  r.AccessToken,
		TokenType:    Bearer, // hmm
		ExpiresIn:    expires,
		RefreshToken: r.RefreshToken,
		Scope:        r.Scope,
		CreatedAt:    created,
	}, nil
}

// UpdateAuth checks if the auth is expired, if not, do nothing
func (oa *OAuth) UpdateAuth() error {
	if !hasCreds() {
		return errors.New("Credentials are empty. Run NewAuth() first")
	}

	expireTime := oa.CreatedAt.Add(oa.ExpiresIn)
	if time.Now().Sub(expireTime) < 0 {
		return nil
	}

	oauth, err := credentials.NewAuth()
	if err != nil {
		return err
	}

	*oa = *oauth

	return nil
}

func hasCreds() bool {
	if credentials.Email == "" || credentials.Password == "" {
		return false
	}

	return true
}
