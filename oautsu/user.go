package oautsu

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"

	kitsu "github.com/KurozeroPB/kitsu-go"
)

// AddEntry adds an anime into the user's library
func (oa *OAuth) AddEntry(anime *kitsu.Anime, episode int) error {
	if !hasCreds() {
		return ErrEmptyCredentials
	}

	err := oa.UpdateAuth()
	if err != nil {
		return err
	}

	status := "current"
	if anime.Attributes.EpisodeCount <= episode {
		status = "completed"
	}

	send := &Library{
		Data: libraryData{
			ID: nil,
			Attributes: attributes{
				Progress: episode,
				Status:   status,
			},
			Relationships: relationships{
				Anime: dataType{
					Data: data{
						Type: "anime",
						ID:   anime.ID,
					},
				},
				User: dataType{
					Data: data{
						Type: "users",
						ID:   strconv.Itoa(User),
					},
				},
			},
			Type: "library-entries",
		},
	}

	JSON, err := json.Marshal(send)
	if err != nil {
		return err
	}

	response, err := doReq(
		bytes.NewBuffer(JSON),
		"POST",
		"https://kitsu.io/api/edge/library-entries",
		oa.AccessToken,
	)

	if err != nil {
		return err
	}

	if response.StatusCode == 422 { // Entry already existed
		library, err := oa.GetEntry(anime)
		if err != nil {
			return err
		}

		id, err := strconv.Atoi(library.Data[0].ID)
		if err != nil {
			return err
		}

		send.Data.ID = &id

		JSON, err := json.Marshal(send)
		if err != nil {
			return err
		}

		response, err = doReq(
			bytes.NewBuffer(JSON),
			"PATCH",
			"https://kitsu.io/api/edge/library-entries/"+library.Data[0].ID,
			oa.AccessToken,
		)
	}

	if response.StatusCode < 200 || response.StatusCode > 210 {
		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return err
		}

		log.Println(string(JSON))
		return errors.New(string(body))
	}

	return nil
}

// GetEntry sends a request to get the current watch entry
func (oa *OAuth) GetEntry(anime *kitsu.Anime) (*UserLibrary, error) {
	if !hasCreds() {
		return nil, ErrEmptyCredentials
	}

	form := url.Values{}
	form.Add("filter[user_id]", strconv.Itoa(User))
	form.Add("filter[anime_id]", anime.ID)
	form.Add("page[limit]", "1")

	response, err := doReq(
		nil,
		"GET",
		"https://kitsu.io/api/edge/library-entries?"+form.Encode(),
		oa.AccessToken,
	)

	if err != nil {
		return nil, err
	}

	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	if response.StatusCode < 200 || response.StatusCode > 210 {
		return nil, errors.New(string(bytes))
	}

	entry := &UserLibrary{}
	if err := json.Unmarshal(bytes, entry); err != nil {
		return nil, err
	}

	if len(entry.Data) < 1 {
		log.Println(string(bytes))
		return nil, errors.New("No data in entry")
	}

	return entry, nil
}

func doReq(body io.Reader, method, endpoint, token string) (*http.Response, error) {
	request, err := http.NewRequest(method, endpoint, body)

	request.Header.Add("Authorization", "Bearer "+token)
	request.Header.Add("Content-Type", "application/vnd.api+json")

	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	return response, nil
}
