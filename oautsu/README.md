# oautsu

Kitsu OAuth API, built on top of `kitsu-go` because I'm lazy

## [`kitsu-go`](https://github.com/KurozeroPB/kitsu-go)

https://github.com/KurozeroPB/kitsu-go

## How-to

1. Make a `Credentials` variable for username and password
2. Run `Credentials.NewAuth()`
3. Set the `oautsu.User` ID to your target username
   - **AGAIN, USER *ID***
4. Run the functions
