package oautsu

import (
	"errors"
	"net/http"
	"time"
)

var (
	client      = &http.Client{}
	credentials = Credentials{}

	// ErrEmptyCredentials is returned when credentials aren't fully initialized
	ErrEmptyCredentials = errors.New("Credentials are empty. Run NewAuth() first")

	// User string to update for
	User int
)

// TokenType for the sake of consistency
type TokenType string

const (
	// Bearer is the only type of Token Type
	Bearer TokenType = "Bearer"
)

// Credentials struct for authenticating
type Credentials struct {
	Email    string
	Password string
}

// OAuth struct to be global
type OAuth struct {
	AccessToken  string
	TokenType    TokenType
	ExpiresIn    time.Duration
	RefreshToken string
	Scope        string
	CreatedAt    time.Time
}

// don't need to bother yourself w/ this
type dummyresponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	Scope        string `json:"scope"`
	CreatedAt    int64  `json:"created_at"`
}

// Library struct contains the data to send
type Library struct {
	Data libraryData `json:"data"`
}

type libraryData struct {
	ID            *int          `json:"id,omitempty"`
	Attributes    attributes    `json:"attributes"`
	Relationships relationships `json:"relationships"`
	Type          string        `json:"type"`
}

type attributes struct {
	Progress int    `json:"progress"`
	Status   string `json:"status,omitempty"`
}

type relationships struct {
	Anime dataType `json:"anime"`
	User  dataType `json:"user"`
}

type dataType struct {
	Data data `json:"data"`
}

type data struct {
	Type string `json:"type"`
	ID   string `json:"id"`
}

// UserLibrary contains the struct received when
// updating/posting/getting/updating the user library
type UserLibrary struct {
	Data []struct {
		ID    string `json:"id"`
		Type  string `json:"type"`
		Links struct {
			Self string `json:"self"`
		} `json:"links"`
		Attributes struct {
			CreatedAt       string      `json:"createdAt"`
			UpdatedAt       string      `json:"updatedAt"`
			Status          string      `json:"status"`
			Progress        int64       `json:"progress"`
			VolumesOwned    int64       `json:"volumesOwned"`
			Reconsuming     bool        `json:"reconsuming"`
			ReconsumeCount  int64       `json:"reconsumeCount"`
			Notes           interface{} `json:"notes"`
			Private         bool        `json:"private"`
			ReactionSkipped string      `json:"reactionSkipped"`
			ProgressedAt    string      `json:"progressedAt"`
			StartedAt       string      `json:"startedAt"`
			FinishedAt      interface{} `json:"finishedAt"`
			Rating          string      `json:"rating"`
			RatingTwenty    interface{} `json:"ratingTwenty"`
		} `json:"attributes"`
		Relationships struct{} `json:"relationships"`
	} `json:"data"`
	Meta struct {
		StatusCounts struct {
			Current int64 `json:"current"`
		} `json:"statusCounts"`
		Count int64 `json:"count"`
	} `json:"meta"`
	Links struct {
		First string `json:"first"`
		Last  string `json:"last"`
	} `json:"links"`
}
